<?php
/**
 * Template Name: 404
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Travel_Log
 */

get_header(); ?>
	
	<div class="row">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<section class="page-content 404">
					<div class="container">
						<div class="row">
							<div class="col-md-12"><div class="section-title">404</div></div>
						</div>
						<div class="row">
							<div class="col-md-12"><div class="page-404-img-wrap"><img src="<?php echo get_stylesheet_directory_uri().'/img/logo_404.png';?>" alt="404" class="not-scale"></div></div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="not-found">This page is not yet available, but you can find </br>something interesting on the <a href="<?php echo   site_url(); ?> ">home page</a></div></div>
						</div>
					</div>
				</section>
			</main><!-- #main -->
		</div><!-- #primary -->

		<?php
		/**
		 * Hook - travel_log_sidebar.
		 *
		 * @hooked travel_log_add_sidebar -  10
		 */
		do_action( 'travel_log_sidebar' );
		?>
	</div>
<?php
get_footer();
