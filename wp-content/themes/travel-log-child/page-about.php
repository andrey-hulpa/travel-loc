<?php
/**
 * Template Name: About page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Travel_Log
 */

get_header(); ?>
	
	<div class="row">
		<div id="primary" class="content-area">
			<<section class="page-content about">
			<div class="row-bg">
				<div class="container">
					<div class="row">
					<div class="col-lg-9 col-md-8 col-sm-12">
						<div class="about-content mission">
							<?php 
								$page_id =  get_post()->ID;
								if (get_field('our_mission_title',$page_id)){
									echo '<h3>'.get_field('our_mission_title',$page_id).'</h3>';
								}
								if (get_field('our_mission_text',$page_id)){
									echo get_field('our_mission_text',$page_id);
								} ?>								
						</div>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-12">
							<ul class="social-group-list">
								<?php 
								if (get_field('social_link_instagram',$page_id)){ ?>
									<li class="social-item instagram"><a href="<?php echo get_field('social_link_instagram',$page_id); ?>">
										<i class="fa fa-instagram" aria-hidden="true"></i>Instagram</a>
									</li>
								<?php }
								if (get_field('social_link_facebook',$page_id)){ ?>
								<li class="social-item facebook"><a href="<?php echo get_field('social_link_facebook',$page_id); ?>">
									<i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>
								</li>
								<?php }
								if (get_field('social_link_tripadvisor',$page_id)){ ?>
									<li class="social-item tripadvisor"><a href="<?php echo get_field('social_link_tripadvisor',$page_id); ?>">
										<span class="icon-tripadvisor"></span>Tripadvisor</a>
									</li>
								<?php } ?>
							</ul>
					</div>
				</div></div>
			</div>
			<div class="row-bg">
				<div class="container">
					<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12">
							<?php 
								$page_id =  get_post()->ID;
								if (get_field('our_story_title',$page_id)){
									echo '<h3>'.get_field('our_story_title',$page_id).'</h3>';
								}
								if (get_field('our_story_text',$page_id)){
									echo get_field('our_story_text',$page_id);
								} ?>		
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12">
							<?php 
							if (get_field('our_story_img',$page_id)){  ?>
								<div class="about-img-wrap about-img-wrap-right">

									<img src="<?php echo get_field('our_story_img',$page_id)['url'];?>" alt="<?php echo get_field('our_story_img',$page_id)['title'];?>" class="about-img">
								</div>
							<?php } ?>
					</div>
					</div>		
				</div>
			</div>
			<div class="row-bg">
				<div class="container">	
					<div class="row">				
						<div class="col-lg-12 col-md-12 col-sm-12">
								<?php 
								if (get_field('why_breathtaking_title',$page_id)){
									echo '<h3>'.get_field('why_breathtaking_title',$page_id).'</h3>';
								} 
								if (get_field('why_breathtaking',$page_id)){
									echo get_field('why_breathtaking',$page_id);
								} ?>							
						</div>
					</div>
				</div>
			</div>
			<div class="row-bg">
				<div class="container">
					<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12">
							<?php 
								if (get_field('love_our_customers_title',$page_id)){
									echo '<h3>'.get_field('love_our_customers_title',$page_id).'</h3>';
								} 
							if (get_field('love_our_customers_text',$page_id)){
									echo get_field('love_our_customers_text',$page_id);
								} ?>	
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<?php 
							if (get_field('love_our_customers_img',$page_id)){  ?>
								<div class="about-img-wrap about-img-wrap-right">
									<img src="<?php echo get_field('love_our_customers_img',$page_id)['url'];?>" alt="<?php echo get_field('love_our_customers_img',$page_id)['title'];?>" class="about-img">
								</div>
							<?php } ?>
						</div>
					</div></div>
				</div>
			<div class="row-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12"> 
							<?php 
								if (get_field('russia_team_title',$page_id)){
									echo '<h3>'.get_field('russia_team_title',$page_id).'</h3>';
								} ?>
							<div class="quote">
								<svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="66px" height="54px" class="quote-icon">
									<path fill-rule="evenodd"  fill="rgb(99, 205, 215)" d="M4.267,49.104 C7.117,52.371 10.596,54.003 14.714,54.003 C18.193,54.003 21.161,52.771 23.617,50.299 C26.069,47.831 27.297,44.844 27.297,41.337 C27.297,37.835 26.228,34.927 24.092,32.614 C21.955,30.308 19.225,29.150 15.901,29.150 C15.426,29.150 14.792,29.233 14.002,29.389 L12.815,29.389 C13.130,25.566 14.792,21.544 17.800,17.322 C20.805,13.102 24.210,9.716 28.009,7.165 L19.225,-0.004 C13.527,4.140 8.897,9.398 5.336,15.768 C1.775,22.141 -0.006,28.833 -0.006,35.842 C-0.006,41.420 1.419,45.841 4.267,49.104 ZM42.254,49.104 C45.103,52.371 48.582,54.003 52.700,54.003 C56.180,54.003 59.188,52.771 61.722,50.299 C64.252,47.831 65.520,44.844 65.520,41.337 C65.520,37.835 64.411,34.927 62.197,32.614 C59.978,30.308 57.211,29.150 53.887,29.150 C53.412,29.150 52.778,29.233 51.988,29.389 L50.801,29.389 C51.276,25.566 52.974,21.544 55.905,17.322 C58.832,13.102 62.197,9.716 65.995,7.165 L57.211,-0.004 C51.513,4.140 46.883,9.398 43.322,15.768 C39.761,22.141 37.980,28.833 37.980,35.842 C37.980,41.420 39.405,45.841 42.254,49.104 Z"/>
								</svg>
								<?php 
								if (get_field('russia_team_text',$page_id)){
									echo get_field('russia_team_text',$page_id);
								} ?>	
							</div>
							<?php 
							if (get_field('russia_team_img',$page_id)){  ?>
								<div class="about-img-wrap">
									<img src="<?php echo get_field('russia_team_img',$page_id)['url'];?>" alt="<?php echo get_field('russia_team_img',$page_id)['title'];?>" class="about-img">
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		</div><!-- #primary -->

		<?php
		/**
		 * Hook - travel_log_sidebar.
		 *
		 * @hooked travel_log_add_sidebar -  10
		 */
		do_action( 'travel_log_sidebar' );
		?>
	</div>
<?php
get_footer();
