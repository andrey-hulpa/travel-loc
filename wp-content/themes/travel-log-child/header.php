<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Travel_Log
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!-- Instruct Internet Explorer to use its latest rendering engine -->
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="travel-site">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php
	$loader_status = travel_log_get_theme_option( 'travel_log_loader' );

	$loader_class = 'loader-inactive';

if ( false === $loader_status ) :

		$loader_class = 'loader-active';

?>
		<div id="onload" class="<?php echo esc_attr( $loader_class );?>" >
			<div id="loader">
				<div id="fountainG_1" class="fountainG"></div>
				<div id="fountainG_2" class="fountainG"></div>
				<div id="fountainG_3" class="fountainG"></div>
				<div id="fountainG_4" class="fountainG"></div>
				<div id="fountainG_5" class="fountainG"></div>
				<div id="fountainG_6" class="fountainG"></div>
				<div id="fountainG_7" class="fountainG"></div>
				<div id="fountainG_8" class="fountainG"></div>
			</div>
		</div>

	<?php endif; ?>

		<div id="page" class="site animate-bottom">
			<div class="top">
			<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'travel-log' ); ?></a>

			

			<?php
			/**
			 * Hook travel_log_header.
			 *
			 * @hooked travel_log_show_header - 10
			 */
			do_action( 'travel_log_header' );
			?>

			<?php 
			/**
			 * Hook after_header.
			 *
			 * @hooked travel_log_404_heading1 - 10
			 */

			do_action( 'after_header_info' ); ?>

			<!-- Breadcrumbs -->

				<?php

				$breadcrumb_status = travel_log_get_theme_option( 'travel_log_breadcrumb' );

				if ( true !== $breadcrumb_status ) :

					if ( ! is_front_page() ) : ?>

						<div id="breadcrumb">
						    <div class="container">

						        <?php

								 	echo breadcrumb_trail($args = array(
										'container' => 'div',
										'show_browse'     => false,
								    ));
								?>

						    </div>
						</div>
				<!-- Breadcrumbs-end -->

					<?php endif;
				endif;
				if (  is_front_page() ) {


					if (get_field('slider_show', '14') ){  ?>
						<section class="slider-header-wrap">
							<div id="slider-header">
							<?php 
							$slider_list = get_field('slider_header_list', '14');
							foreach ($slider_list as $slide) {
								$img_url = get_the_post_thumbnail_url($slide->ID,'full');
								?>
								<div class="slider-header-item" style="background: url(<?php echo $img_url; ?>) no-repeat center; background-size: cover;">
									<div class="slider-title">
									 	<div class="title-wrap">	
									 		<div class="title-img-wrap">
									 			<img src="<?php echo get_stylesheet_directory_uri().'/img/title.png';?>" alt="title" class="title-img">
									 		</div>
										 		<div class="page-desc">Feel like a local, Eat like a local, Love like a local...</div>
										 </div>
								 	</div>
								</div>
							<?php	
							} 
						echo '</div>';

					}}else {
						if ( is_404() ){ 
							$header_bg_url = get_stylesheet_directory_uri().'/img/404_background.jpg';
							$header_title = 'Sorry...';
						}else{
							if (is_page(6)){
								$header_bg_url = get_stylesheet_directory_uri().'/img/destination_backround.jpg';
								$header_title = get_the_title();
							}else {
								if (is_home()){
									$page_id = '17';
								}else{
									$page_id = get_post()->ID;
								}
								$header_bg_url = get_the_post_thumbnail_url($page_id, 'full');
								$header_title = get_the_title();
							}
						}
						?>
						<section class="page page-header-background" style="background: url(<?php echo $header_bg_url; ?>) no-repeat center; background-size: cover;">
	      					<div class="container">
				      			<div class="row">
					      			<h1 class="header-page-title"><?php echo $header_title; ?></h1>
					      		</div>
					  		</div>
						<?php 
					}  	?>
							
					<div class="slider-header-overlay"></div>
					</section>
					<div class="clear-fix"></div>
					
				</div>
