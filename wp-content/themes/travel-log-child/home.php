<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Travel_Log
 */

get_header(); ?>
<section class="page-content blog">
	<div class="container">

	<div class="row">

			<?php
			if ( have_posts() ) : ?>

					<div class="flex-grid">
				<ul class="blog-post-list">
				<?php
				while ( have_posts() ) : the_post();

					?>
					<li class="blog-post-item">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="blog-post-left">
								<div class="blog-img-wrap">
									<?php
									if ( has_post_thumbnail() ) {?>
										<a href="<?php the_permalink() ?>" class="img-wrap">
											<div class="post-overlay"></div>
											<img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'medium' ); ?>" alt="" class="blog-img blog-lg">
										</a>
									<?php } ?>
								</div>
								<div class="post-bottom">
									<div class="post-wrap">
										<div class="blog-post-title titile-primary"><?php the_title(); ?></div>
										<div class="post-except"><?php the_excerpt(); ?></div>
										<a href="<?php esc_url( get_the_permalink() ); ?>" class="post-link link-alt">Read more</a>
									</div>
								</div>
							</div>	
						</article>				
					</li>
					
				<?php

				endwhile; ?>
				<div class="btn-more-posts"><a href="#" class="btn btn-alt">More posts</a></div>
				</ul>
				<?php
				
			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>


		<?php
		/**
		 * Hook - travel_log_sidebar.
		 *
		 * @hooked travel_log_add_sidebar -  10
		 */
		do_action( 'travel_log_sidebar' );
		?></div>
	</div></div>
</section>
<?php
get_footer();
