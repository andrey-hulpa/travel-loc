<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'tr');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pd[e$*_fWOFOd>%DG#8bulv%/UiWmv&I%Yc27?#q}/&uP%hFyf{u5lI; 7R!B7Z,');
define('SECURE_AUTH_KEY',  'nl{[Itkb@.?|kOCy*d?RX9FkN!YWQ)ZlX-Pu&:95w_^[96(-*vn<]&#&1k&Qx#z^');
define('LOGGED_IN_KEY',    'jQjV@pmlBPW)W%riJpNzl{g/q%Hd!SCUYVR%Jn[Htu&F~_TKV4?~sV>m@mY<:FH1');
define('NONCE_KEY',        'k>7:_b 8+Dt^N(N712tCd+%&FGu46  );Q9{@~(gXFQ6Z9Dqk[:Y8):hYqniQCz4');
define('AUTH_SALT',        'Zcq-&u{:bc4S3*(&MS[s0R^E%TAGKRZNW&X5)AF)f{KwU9vncJ2:YoAxXPa:n2)U');
define('SECURE_AUTH_SALT', '6huLrlUYB}H=LA!cCP_b`$&u.AQ*9,n+X=9/YFaF6?{%i&]4QdgU#u3{Q[:H5ciS');
define('LOGGED_IN_SALT',   '?Noz.*nRJ7ZC5pjjD:*ZPv/#<?c)*ZJg<FN4[xw}>mK|`lfNJ5S/wd8m^r7h#blf');
define('NONCE_SALT',       'd{qWywVr1Q26|Cq|EJdVbC`5r]:S^3cMWr#M10#KS0WAM&Up`cY1tO^&vR_7@vgS');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_tr_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
