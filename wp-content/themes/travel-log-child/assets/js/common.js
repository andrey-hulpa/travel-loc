(function($){
$(document).ready(function(){
  $('.menu-bars').click(function(){
      $('.menu-list').toggle();
  });
    $('#quiz-link').click(function(){
       $('.step').hide(); 
       $('#step0').show();

      $('#quiz').show();
  });


var steps = $("#quiz fieldset");
var count = steps.size();
steps.each(function(i) {
  $(this).wrap("<div id='step" + i + "' class='step'></div>");
  $(this).append("<p id='step" + i + "commands' class='step-nav'></p>");
 if (i == 0) {

  selectStep(i);                  // to do
  }
  else if (i == count - 1) {
  $("#step" + i).hide();
  createPrevButton(i);       // to do
  }
  else {
  $("#step" + i).hide();
  createPrevButton(i);       // to do
  createNextButton(i);       // to do
  }
  });
function createPrevButton(i) {
 var stepName = "step" + i;
 $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev'><svg xmlns='http://www.w3.org/2000/svg'  xmlns:xlink='http://www.w3.org/1999/xlink' width='51px' height='48px'><path fill-rule='evenodd'  fill='rgb(188, 196, 202)' d='M51.000,26.000 L6.891,26.000 L26.009,45.119 L23.118,48.009 L-0.009,24.882 L0.872,24.000 L-0.009,23.119 L23.118,-0.009 L26.009,2.882 L6.891,22.000 L51.000,22.000 L51.000,26.000 Z'/></svg></a>");
 $("#" + stepName + "Prev").bind("click", function(e) {
  $("#" + stepName).hide();
  $("#step" + (i - 1)).show();
  selectStep(i - 1);
  });
  }
  $('form').on('click','select, input[type="radio"]' ,function(e){
    e.preventDefault(); 

 //   $('select').on('change', function() {
   //  });
    var el_ID = e.target.id;
    var el = document.getElementById('#'+el_ID);
    i =  parseInt($('#'+el_ID).closest("div.step").attr('id').replace('step',''));

    $("#step" + (i)).hide(); 

    $("#step" + (i + 1)).show(); 
    console.log( i+1);
  })
function createNextButton(i) {
  //var stepName = "step" + i;
  // $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='next'>Next ></a>");
  // $("#" + stepName + "Next").bind("click", function(e) {
  // $("#" + stepName).hide();

  // $("#step" + (i + 1)).show();
  // selectStep(i + 1);
  // });
  }
  function selectStep(i) {
  $("#steps li").removeClass("current");
  $("#stepDesc" + i).addClass("current");
  }

  $('.close-quiz').click(function(){  

    $('#quiz').hide();
  });

	$('#slider-header').bxSlider({
		controls: false,
		speed: 500,
		minSlides: 1, 
		maxSlides: 1, 
		moveSlides: 1
	});	
  $("#gallery-tour").bxSlider({
   		minSlides: 1, 
		maxSlides: 1, 
		moveSlides: 1
	}); 

   


var $sliderFeedback  ;

function sliderFeedbackConfiguration() {
  var windowWidth = $(window).width();
  var numberOfVisibleSlides;

  if (windowWidth < 768) {
    numberOfVisibleSlides = 1;
  }
  else if (windowWidth < 992) {
    numberOfVisibleSlides = 2;
  }
  else if (windowWidth < 1200) {
    numberOfVisibleSlides = 3;
  } else {
    numberOfVisibleSlides = 3;
  }

  return {
    pager: false,
    controls: false,
    auto: true,
    slideWidth: 5000,
    startSlide: 0,
    nextText: ' ',
    prevText: ' ',
    adaptiveHeight: true,
    moveSlides: 1,
    slideMargin: 10,
    minSlides: numberOfVisibleSlides,
    maxSlides: numberOfVisibleSlides
  };
}

function configsliderFeedback() {
  var configsliderFeedback = sliderFeedbackConfiguration();

  if ($sliderFeedback && $sliderFeedback.reloadSlider) {
    $sliderFeedback.reloadSlider(configsliderFeedback);
  }
  else {
    $sliderFeedback = $('#slider-feedback').bxSlider(configsliderFeedback);
  }
}

$(window).on("orientationchange resize", configsliderFeedback);
configsliderFeedback();

var $slider  ;

function buildSliderConfiguration() {
  var windowWidth = $(window).width();
  var numberOfVisibleSlides;

  if (windowWidth < 768) {
    numberOfVisibleSlides = 1;
  }
  else if (windowWidth < 992) {
    numberOfVisibleSlides = 2;
  }
  else if (windowWidth < 1200) {
    numberOfVisibleSlides = 4;
  } else {
    numberOfVisibleSlides = 4;
  }

  return {
    pager: false,
    controls: false,
    auto: true,
    slideWidth: 5000,
    startSlide: 0,
    nextText: ' ',
    prevText: ' ',
    adaptiveHeight: true,
    moveSlides: 1,
    slideMargin: 10,
    minSlides: numberOfVisibleSlides,
    maxSlides: numberOfVisibleSlides
  };
}

function configureSlider() {
  var config = buildSliderConfiguration();

  if ($slider && $slider.reloadSlider) {
    $slider.reloadSlider(config);
  }
  else {
    $slider = $('#slider-photo-gallery').bxSlider(config);
  }
}

$('.slider-prev').click(function () {
  var current = $slider.getCurrentSlide();
  $slider.goToPrevSlide(current) - 1;
});
$('.slider-next').click(function () {
  var current = $slider.getCurrentSlide();
  $slider.goToNextSlide(current) + 1;
});

$(window).on("orientationchange resize", configureSlider);
configureSlider();


	var sliderBestTour = $('#slider-best-tour');
	sliderBestTour.bxSlider({
		controls: false,
		speed: 500,
		slideMargin: 20,
     adaptiveHeight: true,
     responsive: true,
		maxSlides: 1, 
		moveSlides: 1
	});

  $('.best-tour-wrap .prev-tuor').click(function(){
      sliderBestTour.goToPrevSlide();
  });

  $('.best-tour-wrap .next-tuor').click(function(){
      sliderBestTour.goToNextSlide();
      console.log('ddd');
  });


  $('.gallery .prev-tuor').click(function(){
      sliderGallery.goToPrevSlide();
  });

  $('.gallery .next-tuor').click(function(){
      sliderGallery.goToNextSlide();
  });
  var sliderConfirmDate = $('#slider-confirm-date');
  sliderConfirmDate.bxSlider({
    controls: false,
    nav: true,
    speed: 500,
    slideMargin: 10,
    minSlides: 7, 
    maxSlides: 7, 
    moveSlides: 1,
    slideWidth: 120
  });
  


  slider = $('.bxslider').bxSlider();

  //if (typeof magnificPopup == "function"){

  	$('.ajax-popup-link').magnificPopup({
  		  	type: 'ajax'
  		});
  	$('.open-popup-link').magnificPopup({
  		  type:'inline',
  		  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
  	});
    $('.quiz-popup').magnificPopup({
        type:'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });   
    // $('.quiz').magnificPopup({
    //     type:'inline',
    //     midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    // });
    
    
 // }
  

	$( ".plus" ).click( function(){
		$( "#number-travellers" ).val( parseInt( $( "#number-travellers" ).val() ) + 1);
		var count = parseInt( $( "#number-travellers" ).val() );
		var p = parseInt(document.getElementById("price-sale").innerText);
			var subtotal = p * count;
			document.getElementById("subtotal").innerText = subtotal;
			document.getElementById("wp-travel-trip-price_info").innerText = subtotal;
		    jQuery("#spiner").show();
         jQuery.ajax(
            {
              type: "POST",
              dataType: "json",
              url: my_ajax_object.ajax_url,
              data: {
                action: 'person_count_ajax',
                pesoncount: count,
				trip_id: '462',
				arrival_date: '2019-05-30' 
				 
            },
            
            success: function(response){
                console.log(response);
	            jQuery("#spiner").hide();
           	
            }, 
            error: function(errorThrown){
                console.log(errorThrown);
            } 
         });
		
	});	

	$( ".minus" ).click( function(){
		var count = parseInt( $( "#number-travellers" ).val() );
		
		if (count > 1){
			$( "#number-travellers" ).val( parseInt( $( "#number-travellers" ).val() ) - 1);
			var p = parseInt(document.getElementById("price-sale").innerText);
			var subtotal = p * (count-1);
			document.getElementById("subtotal").innerText = subtotal;
			document.getElementById("wp-travel-trip-price_info").innerText = subtotal;
			 jQuery.ajax(
            {
              type: "POST",
              dataType: "json",
              url: my_ajax_object.ajax_url,
              data: {
                action: 'person_count_ajax',
                pesoncount: count-1,
				trip_id: '462',
				arrival_date: '2019-05-30' 
				 
            },
            
            success: function(response){
                console.log(response);
	            jQuery("#spiner").hide();
           	
            }, 
            error: function(errorThrown){
                console.log(errorThrown);
            } 
         });

		}
	});
	
	$(".item-date").click(function(){
		if (!$(this).hasClass('unavailable')){
		
			$(".item-date.active").removeClass('active');
			 $(this).addClass('active');

			var d = $(this).find('.tour-date');
			var p = $(this).find('.tour-price');
			document.getElementById("info-date").innerText = d[0].innerText;
			document.getElementById("price-sale").innerText = p[0].innerText;
			
		}
	});
	
	// $(window).scroll(function(){
	// 	var sticky = $('.header'),
	// 	scroll = $(window).scrollTop();
	// 	if (scroll >= 200) 
	// 		sticky.addClass('sticky');
	// 	  else 
	// 	  	sticky.removeClass('sticky');
	// });



/**********     Datepicker  range     ****** */
$(function () {
  	
var bindDateRangeValidation = function (f, s, e) {
    if(!(f instanceof jQuery)){
			console.log("Not passing a jQuery object");
    }
  
    var jqForm = f,
        startDateId = s,
        endDateId = e;
  
    var checkDateRange = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
          	customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'], 
            })
        }
      
        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);
      
    };

    var hookValidatorEvt = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator();
    hookValidatorEvt();
};
$(function () {
	$('.calendar').click(function(e){
		var duration = $('input[name="trip_duration"]').val();
		var today = moment(cal.selected._i);
		var tomorrow = moment(today).add(duration, 'days');
		$('#trip_date').val(cal.selected._i);
		$('#trip_departure_date').val( moment(tomorrow).format('YYYY-MM-DD'));
		
		
	});
  if (typeof datetimepicker === "function"){
	  console.log('sd');
    var sd = new Date(), ed = new Date();
  
    $('#startDate').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
      defaultDate: sd, 
      minDate: sd 
    });
  
    $('#endDate').datetimepicker({ 
      pickTime: false, 
      format: "YYYY/MM/DD", 
      minDate: sd 
    });

    bindDateRangeValidation($("#form"), 'startDate', 'endDate');
  }
  });

    });
});
})(jQuery);
class Calendar {
  
  constructor () {
    this.monthDiv = document.querySelector('.cal-month__current')
    this.headDivs = document.querySelectorAll('.cal-head__day')
    this.bodyDivs = document.querySelectorAll('.cal-body__day')
    this.nextDiv = document.querySelector('.cal-month__next')
    this.prevDiv = document.querySelector('.cal-month__previous')
  }
  
  init () {
    moment.locale(window.navigator.userLanguage || window.navigator.language) 
    
    this.month = moment()
    this.today = this.selected = this.month.clone()
    this.weekDays = moment.weekdaysShort(true)
    
    this.headDivs.forEach((day, index) => {
      day.innerText = this.weekDays[index]
    })
    if (this.nextDiv){
      this.nextDiv.addEventListener('click', _ => { this.addMonth() })
    }
    if (this.prevDiv){
      this.prevDiv.addEventListener('click', _ => { this.removeMonth() })
    }
    this.bodyDivs.forEach(day => {
      day.addEventListener('click', e => {
        const date = +e.target.innerHTML < 10 ? `0${e.target.innerHTML}` : e.target.innerHTML

        if (e.target.classList.contains('cal-day__month--next')) {
          this.selected = moment(`${this.month.add(1, 'month').format('YYYY-MM')}-${date}`)
        } else if (e.target.classList.contains('cal-day__month--previous')) {
          this.selected = moment(`${this.month.subtract(1, 'month').format('YYYY-MM')}-${date}`)
        } else {
          this.selected = moment(`${this.month.format('YYYY-MM')}-${date}`)
        }

        this.update()
      })
    })
    
    this.update()
  }
  
  update () {
    this.calendarDays = {
      first: this.month.clone().startOf('month').startOf('week').date(),
      last: this.month.clone().endOf('month').date()
    }
    
    this.monthDays = {
      lastPrevious: this.month.clone().subtract(1,'months').endOf('month').date(),
      lastCurrent: this.month.clone().endOf('month').date()
    }
    
    this.monthString = this.month.clone().format('MMMM YYYY')
    
    this.draw()
  }
  
  addMonth () {
    this.month.add(1, 'month')
    
    this.update()
  }
  
  removeMonth () {
    this.month.subtract(1, 'month')
    
    this.update()
  }
  
  draw () {
    if (this.monthDiv){
    this.monthDiv.innerText = this.monthString
  
    let index = 0

    if (this.calendarDays.first > 1) {
      for (let day = this.calendarDays.first; day <= this.monthDays.lastPrevious; index ++) {
        this.bodyDivs[index].innerText = day++

        this.cleanCssClasses(false, index)

        this.bodyDivs[index].classList.add('cal-day__month--previous')
      } 
    }

    let isNextMonth = false
    for (let day = 1; index <= this.bodyDivs.length - 1; index ++) {
      if (day > this.monthDays.lastCurrent) {
        day = 1
        isNextMonth = true
      }

      this.cleanCssClasses(true, index)

      if (!isNextMonth) {
        if (day === this.today.date() && this.today.isSame(this.month, 'day')) {
          this.bodyDivs[index].classList.add('cal-day__day--today') 
        }

        if (day === this.selected.date() && this.selected.isSame(this.month, 'month')) {
          this.bodyDivs[index].classList.add('cal-day__day--selected') 
        }

        this.bodyDivs[index].classList.add('cal-day__month--current')
      } else {
        this.bodyDivs[index].classList.add('cal-day__month--next')
      }

      this.bodyDivs[index].innerText = day++
    }
  }
  }
  
  cleanCssClasses (selected, index) {
    this.bodyDivs[index].classList.contains('cal-day__month--next') && 
      this.bodyDivs[index].classList.remove('cal-day__month--next')
    this.bodyDivs[index].classList.contains('cal-day__month--previous') && 
      this.bodyDivs[index].classList.remove('cal-day__month--previous')
    this.bodyDivs[index].classList.contains('cal-day__month--current') &&
      this.bodyDivs[index].classList.remove('cal-day__month--current')
    this.bodyDivs[index].classList.contains('cal-day__day--today') && 
      this.bodyDivs[index].classList.remove('cal-day__day--today')
    if (selected) {
      this.bodyDivs[index].classList.contains('cal-day__day--selected') && 
        this.bodyDivs[index].classList.remove('cal-day__day--selected') 
    }
  }
}

const cal = new Calendar()
cal.init()