<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Travel_Log
 */

?>
<?php if ( ! is_front_page() ) : ?>
				
	
		</div> <!--container -->


<?php endif; ?>
	
	</div><!-- #container -->
		<footer class="footer_bg">
			<div class="footer-overlay"></div>
			<div class="container">
				<?php
				/**
				 * Hook travel_log_footer_subscription.
				 *
				 * @hooked travel_log_footer_subscription - 10
				 */
				do_action( 'travel_log_footer_subscription' );
				?>
			</div>
			<div class="container">
				<div class="row">
					<div class="footer-bottom">
						<?php
						/**
						 * Hook travel_log_footer.
						 *
						 * @hooked travel_log_footer_logo - 10
						 * @hooked travel_log_footer_menu - 15
						 * @hooked travel_log_social - 20
						 */
						do_action( 'travel_log_footer' );
						?>
					</div>
				</div>
			</div>
		</footer>
	<a href="#page" id="return-to-top"> <i class="fa fa-angle-up"></i></a>
</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
