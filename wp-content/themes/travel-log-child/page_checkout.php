<?php
/**
 * The Template Name: chekout page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Travel_Log
 */

get_header();  
	global $wp_travel_itinerary;
global $wt_cart;
		$trips = $wt_cart->getItems();
var_dump($wt_cart );
		if ( ! $trips ) {
			$wt_cart->cart_empty_message();
			return;
		}
		// Check if login is required for checkout.
		$settings = wp_travel_get_settings();

        $require_login_to_checkout = isset( $settings['enable_checkout_customer_registration'] ) ? $settings['enable_checkout_customer_registration'] : 'no';

        if ( 'yes' === $require_login_to_checkout && ! is_user_logged_in() ) {
            return wp_travel_get_template_part( 'account/form', 'login' );
        }

if ( ! class_exists( 'WP_TRAVEL_ABSPATH' ) ) {
	include_once WP_TRAVEL_ABSPATH . 'inc/framework/form/class.form.php';
}

// Fields array.
$checkout_fields  = wp_travel_get_checkout_form_fields();
$traveller_fields = $checkout_fields['traveller_fields'];
$billing_fields   = $checkout_fields['billing_fields'];
$payment_fields   = $checkout_fields['payment_fields'];
$settings         = wp_travel_get_settings();
	
$currency_code   = ( isset( $settings['currency'] ) ) ? $settings['currency'] : '';
$currency_symbol = wp_travel_get_currency_symbol( $currency_code );

		

$enable_multiple_travellers = isset( $settings['enable_multiple_travellers'] ) ? esc_html( $settings['enable_multiple_travellers'] ) : 'no';
$all_travelers_fields_require = apply_filters( 'wp_travel_require_all_travelers_fields', false );
global $wt_cart;
$form_fw    = new WP_Travel_FW_Form();
$form_field = new WP_Travel_FW_Field();
$form_fw->init_validation( 'wp-travel-booking' );
	
?>
<section class="page-content page-pay">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<form method="POST" action="" class="wp-travel-booking" id="wp-travel-booking">
					<?php do_action( 'wp_travel_action_before_checkout_field' ); ?>
					<!-- Travelers info -->
					<?php
					foreach ( $trips as $cart_id => $trip ) :
						$trip_id        = $trip['trip_id'];
						$trip_price      = wp_travel_get_trip_price( $trip_id );

						$trip_duration = isset( $trip['trip_duration'] ) ? $trip['trip_duration'] : '';

						$pax                = ! empty( $trip['pax'] ) ? $trip['pax'] : 1;
									
						$enable_sale     = get_post_meta( $trip_id, 'wp_travel_enable_sale', true );

						$sale_price      = wp_travel_get_trip_sale_price( $trip_id );
						$trip_start_date = $trip["trip_start_date"] ? $trip["trip_start_date"] : $trip["arrival_date"];


						$price_key      = isset( $trip['price_key'] ) ? $trip['price_key'] : '';
						$pricing_name   = wp_travel_get_trip_pricing_name( $trip_id, $price_key );
						$repeator_count = isset( $trip['pax'] ) ? $trip['pax'] : 1;
						$enable_partial     = $trip['enable_partial'];
						$trip_price_partial = $trip['trip_price_partial'];

						$pax_label = isset( $trip['pax_label'] ) ? $trip['pax_label'] : '';

						$single_trip_total         = wp_travel_get_formated_price( $trip_price * $pax );
						$single_trip_total_partial = wp_travel_get_formated_price( $trip_price_partial * $pax );

						$trip_extras = isset( $trip['trip_extras'] ) ? $trip['trip_extras'] : array();

						$price_per = 'trip-default';
						if ( 'no' === $enable_multiple_travellers ) {
							$repeator_count = 1;
						}
						?>
						<div class="wp-travel-trip-details">
							<?php if ( 'yes' === $enable_multiple_travellers ) : ?>
								<div class="section-title text-left">
									<h3><?php echo esc_html( $pricing_name ); ?><!-- <small> / 8 days 7 nights</small> --></h3>
								</div>
							<?php endif; ?>
							
								<div class="ws-theme-timeline-block panel-group checkout-accordion" id="checkout-accordion-<?php echo esc_attr( $cart_id ); ?>">
									<?php
									$i = 0;
									$start_date = date("d-m-Y");
								//	for ( $i = 0; $i < $repeator_count; $i++ ) :
										?>
										<div class="panel panel-default">
										
											<div class="pay-confirm">
												<div class="pay-section-item">
												<h2 class="pay-section-title">1. Tour snapshot</h2>
												<div class="row">
													<div class="slider-confirm-date">
														<div class="slider-confirm-date" id="slider-confirm-date">
															var_dump();
															<?php 
																for ($i=-2; $i<=30; $i++){ 
																	if ($i<0 ){
																		$day = $i.' day';
																		$price_text = 'Unavailable';
																		$price_class = ' unavailable';
																	}else{
																		
																		$day ='+'.$i.' day';
																		if ($i < 2){
																			$price_text = $sale_price ? $sale_price : $trip_price;
																			$price_text .= wp_travel_get_currency_symbol() ;
																			$price_class = '';
																		}else{

																			$price_text =$trip_price.wp_travel_get_currency_symbol();
																			$price_class = '';
																		}
													$s_date=date('d-m-Y', strtotime( $day ,  strtotime($start_date)));													  $active_date = date('d-m-Y', strtotime( $trip_start_date));
														if ($s_date == $active_date ){ 
															$price_class = ' active';
														}
																	}
																	?>
																	<div id="item-date-<?php echo $i; ?>" 
																		 class="item-date<?php echo $price_class; ?>">
																		
																		<span>
																			<div class="tour-date">
											<?php  echo date('d-m-Y', strtotime( $day ,  strtotime($start_date))); ?>
																			</div>

																			<div class="tour-price"><?php  echo $price_text; ?></div>
																		</span>
																	</div>

															<?php	} ?>

															
														</div>
														
													<div class="confirm-date">
														<ul>
															<li><span class="selected"></span>- Selected day</li>
															<li><span class="not-available"></span>- Not Available</li>
														</ul>
													</div>
												</div></div>

												</div>
												<div>
													<h2 class="pay-section-title">2. Traveller information</h2>	
													<div class="row">
														<div class="col-md-6">
															<?php
																$field=$traveller_fields["gender"];
																$field['label'] = 'Salutation:';
																$field['type'] = 'select';
																$field["options"]["male"]='Mr';
																$field["options"]["female"]='Mrs';
				    
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();


																$field=$traveller_fields["first_name"];
																$field['label'] = 'First name:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();

																$field=$traveller_fields["last_name"];
																$field['label'] = 'Last name:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();

																$field=$traveller_fields["phone_number"];
																$field['label'] = 'Mobile phone:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();

																$field=$traveller_fields["email"];
																$field['label'] = 'Email:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();

																$field=$traveller_fields["reemail"];
																$field['label'] = 'Re-enter email:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();

																$field=$traveller_fields["dob"];
																$field['label'] = 'Date of birth:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render(); 
															?>

														</div>
														<div class="col-md-6"> 
															<?php
																$field=$billing_fields["address"];
																$field['label'] = 'Address:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();

																$field=$billing_fields["billing_city"];
																$field['label'] = 'City:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();														

																$field=$billing_fields["billing_state"];
																$field['label'] = 'State:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();												

																$field=$billing_fields["country"];
																$field['label'] = 'Country:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();			

																$field=$billing_fields["billing_postal"];
																$field['label'] = 'Zip code:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();
															
																$field=$billing_fields["note"];
																$field['label'] = 'Additional request:';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();
																?>
														</div>
													
													</div>
												</div>
												<div class="accept-conditions">
													<h2 class="pay-section-title">3. Accept terms & conditions</h2>	
													<div class="row">
														<div class="col-md-6">
															<?php 
																$field=$traveller_fields["accept"];
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();
															?>										
														</div>
														<div class="col-md-6">
															<?php
																$field=$traveller_fields["subscribe"];
																$field['default'] ='not';
																$field['name'] = sprintf( '%s[%s][%d]', $field['name'], $cart_id, $i );
																$field['id']   = sprintf( '%s-%s-%d', $field['id'], $cart_id, $i );
																$form_field->init( array( $field ) )->render();
															?>
														</div>
													</div>
												</div>
												<div>
												<h2 class="pay-section-title">4. Payment information</h2>	
												<div class="row">
													<div class="total">Total amount: <?php echo wp_travel_get_currency_symbol(); ?></span>
														<span id="subtotal" class="product-total-price amount" ><?php echo esc_html( $single_trip_total ); ?></span></div>
			<div class="img-wrap not-hover">
				<img src="<?php echo get_stylesheet_directory_uri(). '/img/pay-card.jpg'; ?>" alt="">
				<div id="payment-error" class="alert alert-danger hidden"></div>

<form action="/examples/custom-form/payment" method="post" id="payment-form" class="form-horizontal"><input type="hidden" name="_csrf" value="c99372e2-78fd-46b0-9191-e25734c39948">
    <!-- custom fields can be added here -->        <div class="form-group">
        
        <div class="col-sm-9">
            <input data-securionpay="number" class="form-control card" type="text">
        </div>
    </div>
    <div class="form-group">
        
        <div class="col-sm-4 col-md-9 col-lg-4 expiration">
            <input data-securionpay="expMonth" class="form-control month" type="text" placeholder="" size="2">
            <input data-securionpay="expYear" class="form-control year" type="text" placeholder="" size="2">
        </div>

        
        <div class="col-sm-3">
            <input data-securionpay="cvc" class="form-control cvc" type="text" size="4">
        </div> 
		</div>
	    <div class="form-group">
			<div class="col-md-12">
				<input data-securionpay="namecard" class="form-control namecard" type="text" size="30">
			</div>
	    </div>
	<div class="col-md-12 politic">Note: Shortly, after completing your payment, you will receive your tour confirmation voucher to the email address you provided. Sometimes it can end up in the SPAM folder so please check there (and your Promotions tab if you're using Gmail!). If the voucher is missing in action after a couple of hours, please contact us at info@urbanadventures.com. and we'll organise a search party (or just send you another one).</div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary btn-lg">Buy now</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="https://securionpay.com/js/securionpay.js"></script>
<script type="text/javascript">
    /*<![CDATA[*/
    // This is required to identify your account
    Securionpay.setPublicKey("pu_test_WVMFC9GFuvm54b0uorifKkCh");

    $(function () {
        var $form = $('#payment-form');

        $form.submit(paymentFormSubmit);

        function paymentFormSubmit(e) {
            // Prevent the form from submitting default action
            e.preventDefault();

            // Disable form submit button to prevent repeatable submits
            $form.find('button').prop('disabled', true);

            // Send card data to SecurionPay
            SecurionPay.createCardToken($form, createCardTokenCallback);
        }

        function createCardTokenCallback(token) {
            // Check for errors
            if (token.error) {
                // Display error message
                $('#payment-error').text(token.error.message).removeClass('hidden');
                // Re-enable form submit button
                $form.find('button').prop('disabled', false);
            } else {
                // Open frame with 3-D Secure process
                SecurionPay.verifyThreeDSecure({
                    amount: 2499,
                    currency: 'EUR',
                    card: token.id
                }, verifyThreeDSecureCallback);
            }
        }

        function verifyThreeDSecureCallback(token) {
            // Check for errors
            if (token.error) {
                // Display error message
                $('#payment-error').text(token.error.message).removeClass('hidden');
                // Re-enable form submit button
                $form.find('button').prop('disabled', false);
            } else {
                // Append token to the form so that it will be send to server
                $form.append($('<input type="hidden" name="tokenId" />').val(token.id));
                $form.append($('<input type="hidden" name="requireEnrolledCard" />').val($("#require-enrolled-card").prop('checked')));
                $form.append($('<input type="hidden" name="requireSuccessfulLiabilityShift" />').val($("#require-successful-liability-shift").prop('checked')));

                // Submit the form with its default action
                $form.unbind();
                $form.submit();
            }
        }
    });
    /*]]>*/
</script>
			</div>
													
												</div>
												
											</div>
										</div>
									<?php //endfor; ?>
								
							</div>
							
							</div>
						</div>
					<?php endforeach; ?>

				
				</form>		
			</div>
			<div class="col-md-3 side-bar">
				<div class="widget-wrap">
					<div class="widget-title"><?php echo esc_html( $pricing_name ); ?></div>
					<div class="widget-subtitle"><?php echo get_field('district', $trip_id); ?></div>
					<div class="img-wrap">
						<?php $imgurl=get_the_post_thumbnail_url($trip_id,'full'); ?>
						<img src="<?php echo  $imgurl; ?>" alt="tour-img">
					</div>
					<div class="info-type-tour">
						<table>
							<tr>
								<td class="info-title">Your date:</td>
								<td id="info-date"class="info-desc"><?php echo date('d-m-Y', strtotime( $trip_start_date)); ?></td>
							</tr>
							<tr>
								<td class="info-title">Pick-up time:</td>
								<td class="info-desc"><?php echo get_field('pick-up_time', $trip_id); ?></td>
							</tr>
							<tr>
								<?php 
								$wp_travel_trip_facts = get_post_meta( $trip_id, 'wp_travel_trip_facts', true );
								foreach ($wp_travel_trip_facts  as $fact ) {
									if ( $fact['label'] == 'Duration'){
										$fact_duration = $fact['value'];
									}
								} ?>
								<td class="info-title">Duration:</td>
								<td class="info-desc"><?php echo $fact_duration; ?></td>
							</tr>
						</table>
					</div>
					<div class="row">
						<div class="number-travellers">
							<div class="inupt-group">
									<label for="number-travellers" class="number-travellers">How many travellers:</label>
									<div class="right-number">
										<span class="plus"></span>
										<input type="number" id="number-travellers" name="number-travellers" class="form-control" value="<?php echo esc_attr( $pax ); ?>" minvalue="1">
										<span class="minus"></span>
									</div>
								</div>
						</div>
						<div class="meta-info">
							<div class="price-regular-wrap">
								<?php 
									$sale_price = $sale_price ? $sale_price : $trip_price ; 
								?>
								
									<span id="price-sale" class="price-sale"> <?php echo apply_filters( 'wp_travel_itinerary_sale_price', sprintf( ' %s %s', $sale_price , $currency_symbol), $currency_symbol, $sale_price  ); ?></span>
								<?php 	if ( $enable_sale ) {  ?>

									<span class="price-regular"><?php echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ',  $trip_price,$currency_symbol ), $currency_symbol, $trip_price ); ?></span>
								<?php  } ?>
								</div>
							<?php if ( $enable_sale ) : ?>

								<div class="discount-wrap">
										<span class="discount"><?php echo round(100-($sale_price/$trip_price)*100); ?>%</span>
										<span class="discount-exit-date">Discount is availabel untill <?php var_dump($end_date); ?> </span>
								</div>

							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();
