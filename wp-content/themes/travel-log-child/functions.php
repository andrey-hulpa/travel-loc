<?php
/**
 *Recommended way to include parent theme styles.
 *(Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
 */  
  add_action( 'wp_enqueue_scripts', 'travel_log_child_style' );
  function travel_log_child_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/style.css',array('parent-style'));
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/main.min.css');
   	wp_enqueue_script( 'bxslider', 'https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js', array( 'jquery' ),'1',true );
   	wp_enqueue_script( 'retina', get_stylesheet_directory_uri() . '/assets/libs/retina/retina.min.js', array( 'jquery' ),'1',true );
   	wp_enqueue_script( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/libs/lightbox/jquery.magnific-popup.min.js', array( 'jquery' ),'1',true );
   	wp_enqueue_script( 'moment', get_stylesheet_directory_uri() . '/assets/libs/moment/moment-with-locales.js', array( 'jquery' ),'1',true );
   	wp_enqueue_script( 'travel-custom', get_stylesheet_directory_uri() . '/assets/js/common.js', array( 'jquery' ),'1',true );

  

}

// Register Custom Post Type Header-slider
function create_headerslider_cpt() {

	$labels = array(
		'name' => _x( 'Header-slider', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Header-slider', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Header-slider', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Header-slider', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Header-slider Archives', 'textdomain' ),
		'attributes' => __( 'Header-slider Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Header-slider:', 'textdomain' ),
		'all_items' => __( 'All Header-slider', 'textdomain' ),
		'add_new_item' => __( 'Add New Header-slider', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Header-slider', 'textdomain' ),
		'edit_item' => __( 'Edit Header-slider', 'textdomain' ),
		'update_item' => __( 'Update Header-slider', 'textdomain' ),
		'view_item' => __( 'View Header-slider', 'textdomain' ),
		'view_items' => __( 'View Header-slider', 'textdomain' ),
		'search_items' => __( 'Search Header-slider', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Header-slider', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Header-slider', 'textdomain' ),
		'items_list' => __( 'Header-slider list', 'textdomain' ),
		'items_list_navigation' => __( 'Header-slider list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Header-slider list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Header-slider', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => '',
		'supports' => array('title', 'excerpt', 'thumbnail'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'headerslider', $args );

}
add_action( 'init', 'create_headerslider_cpt', 0 );

remove_action( 'travel_log_header' , 'travel_log_show_header', 10);


if ( ! function_exists( 'travel_log_show_header' ) ) :
	/**
	 * Site header section.
	 *
	 * @since 1.0.0
	 */
	function travel_log_show_header() {
		$header_image       = get_custom_header();
		$header_image_style = '';
		$header_image_class = '';
		if ( '' !== $header_image->url ) {
			$header_image_style = 'background-image: url(' . $header_image->url . ');background-position: 50%;background-size: cover;';
			$header_image_class = 'site-header-image';
		}
		?>
		<header id="masthead" class="site-header <?php echo esc_attr( $header_image_class ); ?>" role="banner" style="<?php echo esc_attr( $header_image_style ); ?>">
	<div class="container">
		
		<?php
		do_action( 'travel_log_after_header_container_open' );
		?>
		<div class="header-main-menu header-top">
			<div class="site-branding logo-wrap">
				<?php
				/**
				 * Hook travel_log_site_branding.
				 *
				 * @hooked travel_log_site_identity - 10
				 */
				do_action( 'travel_log_site_branding' );
				?>
			</div><!-- .site-branding -->

				<div id="main-nav" class="header-menu">
				<?php
				/**
				 * Hook travel_log_main_nav.
				 *
				 * @hooked travel_log_main_menu - 10
				 */
				do_action( 'travel_log_main_nav' );
				?>
				</div>
				<div id="header-search" class="search-form">
				<a href="#search-form">
					<i class="fa fa-search"></i>
				</a>
				<div id="search-form">
					<span class="close"><i class="fa fa-times"></i></span>
						<?php
						$header_itinerary_search = travel_log_get_theme_option( 'travel_log_enable_header_wp_travel_search' );

						if ( class_exists( 'WP_Travel' ) && true === $header_itinerary_search ) { ?>
							<section id="section-itinerary-search" class="section-itinerary-search">
								<div class="container">
									<div class="row">
										<div class="col-sm-12">
											<?php wp_travel_search_form(); ?>
										</div>
									</div>
								</div>
							</section>
						<?php
						} else {
							// Get Default Search Form.
							get_search_form();
						}
						?>
				</div>
				</div>
				<div class="social">
					<?php
						/**
						 * Hook travel_log_top_header_right.
						 *
						 * @hooked travel_log_top_header_right_content - 10
						 */
						do_action( 'travel_log_top_header_right' );
						?>
				</div>

				</div>
				<?php
				/**
				 * Hook travel_log_before_header_container_close.
				 */
				do_action( 'travel_log_before_header_container_close' );

				?>
			</div>
		</header><!-- #masthead -->
		<?php
	}

endif;

add_action( 'travel_log_header', 'travel_log_show_header' );


if ( ! function_exists( 'travel_log_site_identity' ) ) :

	/**
	 * Show site logo or title in header.
	 *
	 * @since 1.0.0
	 */
	function travel_log_site_identity() {
		?>
		<div id="site-identity">
			<?php

			if ( has_custom_logo() ) {
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home" itemprop="url">
					<?php the_custom_logo(); ?>
				</a>
			<?php
			} ?>
			<div class="site-branding-text">
				<?php if ( is_front_page() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php endif; ?>
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}

endif;

add_action( 'travel_log_site_branding', 'travel_log_site_identity' );


if ( ! function_exists( 'travel_log_main_menu' ) ) :

	/**
	 * Show Main menu in header.
	 *
	 * @since 1.0.0
	 */
	function travel_log_main_menu() {
		?>
		<nav id="site-navigation" class="main-navigation" role="navigation">
		<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'travel-log' ); ?></button>
		<div class="wrap-menu-content">
			<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_id' => 'primary-menu', 'menu_class' => 'menu menu-list', 'container_class' => 'menu-header-menu-container', 'fallback_cb' => 'travel_log_primary_menu_fallback' ) ); ?>
		</div>
		</nav>
		<?php
	}

endif;

add_action( 'travel_log_main_nav', 'travel_log_main_menu' );



if ( ! function_exists( 'travel_log_entry_header' ) ) :
	/**
	 * Prints title and post date.
	 */
	function travel_log_entry_header() {
		the_title('<h1 class="simple-post-title">', '</a></h1>' ); ?>

		
		<?php
	}
	endif;


if ( ! function_exists( 'travel_log_top_header_right_content' ) ) :

	/**
	 * Top header right content.
	 *
	 * @since 1.0.0
	 */
	function travel_log_top_header_right_content() { ?>
		<ul class="social-list">
			<li class="social-item"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li class="social-item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li class="social-item"><a href="#"><span class="icon-tripadvisor"></span></a></li>
		</ul>
	<?php }

endif;

add_action( 'travel_log_top_header_right', 'travel_log_top_header_right_content' );
	function travel_log_footer_subscription() { ?>
				<div class="row">
					<h2 class="footer_title">
						Sign up for promotions and weekly Breathtaking Russia
					</h2>
				</div>
				<div class="row">
					<div class="input-group-email">
					  <input type="email" class="form-control" placeholder="Enter email address" aria-label="Enter email address">
					  <div class="input-group-append">
					    <button class="btn btn-primary btn-lg" type="button" id="button-addon2">Sign up</button>
					  </div>
					</div>
				</div>
	<?php }

add_action( 'travel_log_footer_subscription', 'travel_log_footer_subscription' );

function travel_log_footer_logo() { 

	if ( has_custom_logo() ) {
		?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link logo" rel="home" itemprop="url">
			<img src="<?php echo get_stylesheet_directory_uri().'/img/logo-footer.png';?> " alt="logo">
		</a>
	<?php 
	} 
}

add_action( 'travel_log_footer', 'travel_log_footer_logo' );

function travel_log_footer_menu() { ?>
	<div class="footer-menu">
		<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'quick-menu footer1', 'fallback_cb' => 'travel_log_primary_menu_fallback' ) ); ?>
	</div>
<?php 
}

add_action( 'travel_log_footer', 'travel_log_footer_menu' );
						
function travel_log_social() { ?>
	<div class="social">
		<ul class="social-list">
			<li class="social-item"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li class="social-item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li class="social-item"><a href="#"><span class="icon-tripadvisor"></span></a></li>
		</ul>
	</div>
<?php 
}

add_action( 'travel_log_footer', 'travel_log_social' );


remove_action( 'wp_travel_booking_princing_options_list' , 'wp_travel_booking_tab_pricing_options_list', 10);


function wp_travel_booking_tab_pricing_options_list_new( $trip_data ) {

	if ( '' == $trip_data ) {
		return;
	}
	global $wp_travel_itinerary;

	if ( is_array( $trip_data ) ) {
		global $post;
		$trip_id = $post->ID;
	} elseif ( is_numeric( $trip_data ) ) {
		$trip_id = $trip_data;
	}

	$js_date_format = wp_travel_date_format_php_to_js();

	$settings   = wp_travel_get_settings();
	$form       = new WP_Travel_FW_Form();
	$form_field = new WP_Travel_FW_Field();

	$fixed_departure = get_post_meta( $trip_id, 'wp_travel_fixed_departure', true );
	$show_end_date   = wp_travel_booking_show_end_date();
	$currency_code   = ( isset( $settings['currency'] ) ) ? $settings['currency'] : '';
	$currency_symbol = wp_travel_get_currency_symbol( $currency_code );

	$trip_start_date = get_post_meta( $trip_id, 'wp_travel_start_date', true );
	$trip_end_date   = get_post_meta( $trip_id, 'wp_travel_end_date', true );
	$trip_price      = wp_travel_get_trip_price( $trip_id );
	$enable_sale     = get_post_meta( $trip_id, 'wp_travel_enable_sale', true );

	$trip_duration       = get_post_meta( $trip_id, 'wp_travel_trip_duration', true );
	$trip_duration       = ( $trip_duration ) ? $trip_duration : 0;
	$trip_duration_night = get_post_meta( $trip_id, 'wp_travel_trip_duration_night', true );
	$trip_duration_night = ( $trip_duration_night ) ? $trip_duration_night : 0;

	$per_person_text = wp_travel_get_price_per_text( $trip_id );
	$sale_price      = wp_travel_get_trip_sale_price( $trip_id );

	$available_pax    = false;
	$booked_pax       = false;
	$pax_limit        = false;
	$general_sold_out = false;

	$status_col = apply_filters( 'wp_travel_inventory_enable_status_column', false, $trip_id );

	$status_msg           = get_post_meta( $trip_id, 'wp_travel_inventory_status_message_format', true );
	$sold_out_btn_rep_msg = apply_filters( 'wp_travel_inventory_sold_out_button', '', $trip_id );

	// Multiple Pricing.
	if ( is_array( $trip_data ) ) {
		if ( empty( $trip_data ) ) {
			return;
		}
		?>
		<div id="wp-travel-date-price" class="detail-content">
			<div class="availabily-wrapper">
				<ul class="availabily-list additional-col">
	
					<?php
					foreach ( $trip_data as $price_key => $pricing ) :
						// Set Vars.
						$pricing_name         = isset( $pricing['pricing_name'] ) ? $pricing['pricing_name'] : '';
						$price_key            = isset( $pricing['price_key'] ) ? $pricing['price_key'] : '';
						$pricing_type         = isset( $pricing['type'] ) ? $pricing['type'] : '';
						$pricing_custom_label = isset( $pricing['custom_label'] ) ? $pricing['custom_label'] : '';
						$pricing_option_price = isset( $pricing['price'] ) ? $pricing['price'] : '';
						$pricing_sale_enabled = isset( $pricing['enable_sale'] ) ? $pricing['enable_sale'] : '';
						$pricing_sale_price   = isset( $pricing['sale_price'] ) ? $pricing['sale_price'] : '';
						$pricing_min_pax      = isset( $pricing['min_pax'] ) ? $pricing['min_pax'] : '';
						$pricing_max_pax      = isset( $pricing['max_pax'] ) ? $pricing['max_pax'] : '';

						$available_dates = wp_travel_get_trip_available_dates( $trip_id, $price_key ); // No need to pass date

						$pricing_sold_out = false;

						$inventory_data = array(
							'status_message' => __( 'N/A', 'wp-travel' ),
							'sold_out'       => false,
							'available_pax'  => 0,
							'booked_pax'     => 0,
							'pax_limit'      => 0,
							'min_pax'        => $pricing_min_pax,
							'max_pax'        => $pricing_max_pax,
						);

						if ( is_array( $available_dates ) && count( $available_dates ) > 0 ) { // multiple available dates
							foreach ( $available_dates as $available_date ) {
								// echo $available_date;
								$inventory_data = apply_filters( 'wp_travel_inventory_data', $inventory_data, $trip_id, $price_key, $available_date ); // Need to pass inventory date to get availability as per specific date.

								$pricing_status_msg = $inventory_data['status_message'];
								$pricing_sold_out   = $inventory_data['sold_out'];
								$available_pax      = $inventory_data['available_pax'];
								$booked_pax         = $inventory_data['booked_pax'];
								$pax_limit          = $inventory_data['pax_limit'];
								$min_pax            = $inventory_data['min_pax'];
								$max_pax            = $inventory_data['max_pax'];

								if ( class_exists( 'WP_Travel_Util_Inventory' ) ) {
									$inventory = new WP_Travel_Util_Inventory();
									if ( $inventory->is_inventory_enabled( $trip_id ) && $available_pax ) {
										$pricing_max_pax = $available_pax;
									}
								}
								$max_attr  = 'max=' . $pricing_max_pax;
								$parent_id = sprintf( 'pricing-%s-%s', esc_attr( $price_key ), $available_date );

								$unavailable_class = '';
								$availability      = wp_travel_trip_availability( $trip_id, $price_key, $available_date, $pricing_sold_out );
								if ( ! $availability ) {
									$unavailable_class = 'pricing_unavailable';
								}
								?>
								<li id="<?php echo esc_html( $parent_id ); ?>" class="availabily-content clearfix <?php echo esc_attr( $unavailable_class ); ?>">
									<div class="date-from">
										<span class="availabily-heading-label"><?php echo esc_html__( 'Pricing Name:', 'wp-travel' ); ?></span>
										<span> <?php echo esc_html( $pricing_name ); ?> </span>
									</div>
									<div class="date-from">
										<span class="availabily-heading-label"><?php echo esc_html__( 'Start:', 'wp-travel' ); ?></span>
										<span> <?php echo esc_html( wp_travel_format_date( $available_date ) ); ?> </span>
									</div>
									<div class="status">
										<span class="availabily-heading-label"><?php echo esc_html__( 'Min Group Size:', 'wp-travel' ); ?></span>
										<span><?php echo ! empty( $pricing_min_pax ) ? esc_html( $pricing_min_pax . __( ' pax', 'wp-travel' ) ) : esc_html__( 'No size limit', 'wp-travel' ); ?></span>
									</div>
									<div class="status">
										<span class="availabily-heading-label"><?php echo esc_html__( 'Max Group Size:', 'wp-travel' ); ?></span>
										<span><?php echo ! empty( $pricing_max_pax ) ? esc_html( $pricing_max_pax . __( ' pax', 'wp-travel' ) ) : esc_html__( 'No size limit', 'wp-travel' ); ?></span>
									</div>
									<?php
									if ( $status_col ) :

										if ( $pricing_sold_out ) :
											?>
											<div class="status">
												<span class="availabily-heading-label"><?php echo esc_html__( 'Status:', 'wp-travel' ); ?></span>
												<span><?php echo esc_html__( 'SOLD OUT', 'wp-travel' ); ?></span>
											</div>
										<?php else : ?>
											<div class="status">
												<span class="availabily-heading-label"><?php echo esc_html__( 'Status:', 'wp-travel' ); ?></span>
												<span><?php echo esc_html( $pricing_status_msg ); ?></span>

											</div>
											<?php
										endif;
									endif;
									?>
									<div class="price">
										<span class="availabily-heading-label"><?php echo esc_html__( 'price:', 'wp-travel' ); ?></span>
										<?php if ( '' !== $pricing_option_price || '0' !== $pricing_option_price ) : ?>

											<?php if ( 'yes' === $pricing_sale_enabled ) : ?>
												<del>
													<span><?php echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ', $currency_symbol, $pricing_option_price ), $currency_symbol, $pricing_option_price ); ?></span>
												</del>
											<?php endif; ?>
												<span class="person-count">
													<ins>
														<span>
															<?php
															if ( 'yes' === $pricing_sale_enabled ) {
																echo apply_filters( 'wp_travel_itinerary_sale_price', sprintf( ' %s %s', $currency_symbol, $pricing_sale_price ), $currency_symbol, $pricing_sale_price );
															} else {
																echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ', $currency_symbol, $pricing_option_price ), $currency_symbol, $pricing_option_price );
															}
															?>
														</span>
													</ins>/<?php echo esc_html( $per_person_text ); ?>
												</span>
										<?php endif; ?>
									</div>
									<div class="action">
		
										<?php if ( $pricing_sold_out ) : ?>
		
											<p class="wp-travel-sold-out"><?php echo $sold_out_btn_rep_msg; ?></p>
		
										<?php else : ?>
											<a href="#0" class="btn btn-primary btn-sm btn-inverse show-booking-row"><?php echo esc_html__( 'Select', 'wp-travel' ); ?></a>
										<?php endif; ?>
									</div>
									<?php if ( $availability ) : // Remove Book now if trip is soldout. ?>
										<div class="wp-travel-booking-row">
											<?php
												/**
												 * Support For WP Travel Tour Extras Plugin.
												 *
												 * @since 1.5.8
												 */
												do_action( 'wp_travel_trip_extras', $price_key, $available_date );
											?>
											<div class="wp-travel-calender-aside">
												<?php
												$pricing_default_types = wp_travel_get_pricing_variation_options();

												$pricing_type_label = ( 'custom' === $pricing_type ) ? $pricing_custom_label : $pricing_default_types[ $pricing_type ];

												$max_attr = '';
												$min_attr = 'min=1';
												if ( '' !== $pricing_max_pax ) {

													$max_attr = 'max=' . $pricing_max_pax;
												}
												if ( '' !== $pricing_min_pax ) {
													$min_attr = 'min=' . $pricing_min_pax;
												}

												?>
												<div class="col-sm-3">
													<label for=""><?php echo esc_html( ucfirst( $pricing_type_label ) ); ?></label>
													<input name="pax" type="number" <?php echo esc_attr( $min_attr ); ?> <?php echo esc_attr( $max_attr ); ?> placeholder="<?php echo esc_attr__( 'size', 'wp-travel' ); ?>" required data-parsley-trigger="change">
												</div>
												<div class="add-to-cart">
													<input type="hidden" name="trip_date" value="<?php echo esc_attr( $available_date ); ?>" >
													<input type="hidden" name="trip_id" value="<?php echo esc_attr( get_the_ID() ); ?>" />
													<input type="hidden" name="price_key" value="<?php echo esc_attr( $price_key ); ?>" />
													<?php
														$button   = '<a href="%s" data-parent-id="' . $parent_id . '" class="btn add-to-cart-btn btn-primary btn-sm btn-inverse">%s</a>';
														$cart_url = add_query_arg( 'trip_id', get_the_ID(), wp_travel_get_cart_url() );
													if ( 'yes' !== $fixed_departure ) :
														$cart_url = add_query_arg( 'trip_duration', $trip_duration, $cart_url );
														?>
															<input type="hidden" name="trip_duration" value="<?php echo esc_attr( $trip_duration ); ?>" />
														<?php
														endif;

														$cart_url = add_query_arg( 'price_key', $price_key, $cart_url );
														printf( $button, esc_url( $cart_url ), esc_html__( 'Book now', 'wp-travel' ) );
													?>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</li>
								<?php
							}
						} else {
							$inventory_data = apply_filters( 'wp_travel_inventory_data', $inventory_data, $trip_id, $price_key ); // Need to pass inventory date to get availability as per specific date.

							$pricing_status_msg = $inventory_data['status_message'];
							$pricing_sold_out   = $inventory_data['sold_out'];
							$available_pax      = $inventory_data['available_pax'];
							$booked_pax         = $inventory_data['booked_pax'];
							$pax_limit          = $inventory_data['pax_limit'];
							$min_pax            = $inventory_data['min_pax'];
							$max_pax            = $inventory_data['max_pax'];

							if ( class_exists( 'WP_Travel_Util_Inventory' ) ) {
								$inventory = new WP_Travel_Util_Inventory();
								if ( $inventory->is_inventory_enabled( $trip_id ) && $available_pax ) {
									$pricing_max_pax = $available_pax;
								}
							}
							$max_attr = 'max=' . $pricing_max_pax;
							?>
							<li id="pricing-<?php echo esc_attr( $price_key ); ?>" class="availabily-content clearfix">
								<div class="date-from">
									<span class="availabily-heading-label"><?php echo esc_html__( 'Pricing Name:', 'wp-travel' ); ?></span> <span><?php echo esc_html( $pricing_name ); ?></span>
								</div>
								<div class="date-from">
									<span class="availabily-heading-label"><?php echo esc_html__( 'Start:', 'wp-travel' ); ?></span>
									<span></span>
								</div>
								<div class="status">
									<span class="availabily-heading-label"><?php echo esc_html__( 'Min Group Size:', 'wp-travel' ); ?></span>
									<span><?php echo ! empty( $pricing_min_pax ) ? esc_html( $pricing_min_pax . __( ' pax', 'wp-travel' ) ) : esc_html__( 'No size limit', 'wp-travel' ); ?></span>
								</div>
								<div class="status">
									<span class="availabily-heading-label"><?php echo esc_html__( 'Max Group Size:', 'wp-travel' ); ?></span>
									<span><?php echo ! empty( $pricing_max_pax ) ? esc_html( $pricing_max_pax . __( ' pax', 'wp-travel' ) ) : esc_html__( 'No size limit', 'wp-travel' ); ?></span>
								</div>
								<?php
								if ( $status_col ) :

									if ( $pricing_sold_out ) :
										?>
										<div class="status">
											<span class="availabily-heading-label"><?php echo esc_html__( 'Status:', 'wp-travel' ); ?></span>
										</div>
									<?php else : ?>
										<div class="status">
											<span class="availabily-heading-label"><?php echo esc_html__( 'Status:', 'wp-travel' ); ?></span>
										</div>
										<?php
									endif;
														endif;
								?>
								<div class="price">
									<span class="availabily-heading-label"><?php echo esc_html__( 'price:', 'wp-travel' ); ?></span>
									<?php if ( '' !== $pricing_option_price || '0' !== $pricing_option_price ) : ?>
	
										<?php if ( 'yes' === $pricing_sale_enabled ) : ?>
											<del>
												<span><?php echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ', $currency_symbol, $pricing_option_price ), $currency_symbol, $pricing_option_price ); ?></span>
											</del>
										<?php endif; ?>
											<span class="person-count">
												<ins>
													<span>
														<?php
														if ( 'yes' === $pricing_sale_enabled ) {
															echo apply_filters( 'wp_travel_itinerary_sale_price', sprintf( ' %s %s', $currency_symbol, $pricing_sale_price ), $currency_symbol, $pricing_sale_price );
														} else {
															echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ', $currency_symbol, $pricing_option_price ), $currency_symbol, $pricing_option_price );
														}
														?>
													</span>
												</ins>/<?php echo esc_html( $per_person_text ); ?>
											</span>
									<?php endif; ?>
								</div>
								<div class="action">
	
									<?php if ( $pricing_sold_out ) : ?>
	
										<p class="wp-travel-sold-out"><?php echo $sold_out_btn_rep_msg; ?></p>
	
									<?php else : ?>
										<a href="#0" class="btn btn-primary btn-sm btn-inverse show-booking-row"><?php echo esc_html__( 'Select', 'wp-travel' ); ?></a>
									<?php endif; ?>
								</div>
								<div class="wp-travel-booking-row">
									<?php
										
										do_action( 'wp_travel_trip_extras', $price_key );
									?>
									<div class="wp-travel-calender-column no-padding ">
	
										<label for=""><?php echo esc_html__( 'Select a Date:', 'wp-travel' ); ?></label>
										<input data-date-format="<?php echo esc_attr( $js_date_format ); ?>" name="trip_date" type="text" data-available-dates="<?php echo ( $available_dates ) ? esc_attr( wp_json_encode( $available_dates ) ) : ''; ?>" readonly class="wp-travel-pricing-dates" required data-parsley-trigger="change" data-parsley-required-message="<?php echo esc_attr__( 'Please Select a Date', 'wp-travel' ); ?>">
	
									</div>
									<div class="wp-travel-calender-aside">
										<?php
										$pricing_default_types = wp_travel_get_pricing_variation_options();

										$pricing_type_label = ( 'custom' === $pricing_type ) ? $pricing_custom_label : $pricing_default_types[ $pricing_type ];

										$max_attr = '';
										$min_attr = 'min=1';
										if ( '' !== $pricing_max_pax ) {

											$max_attr = 'max=' . $pricing_max_pax;
										}
										if ( '' !== $pricing_min_pax ) {
											$min_attr = 'min=' . $pricing_min_pax;
										}

										?>
										<div class="col-sm-3">
											<label for=""><?php echo esc_html( ucfirst( $pricing_type_label ) ); ?></label>
											<input name="pax" type="number" <?php echo esc_attr( $min_attr ); ?> <?php echo esc_attr( $max_attr ); ?> placeholder="<?php echo esc_attr__( 'size', 'wp-travel' ); ?>" required data-parsley-trigger="change">
										</div>
										<div class="add-to-cart">
											<input type="hidden" name="trip_id" value="<?php echo esc_attr( get_the_ID() ); ?>" />
											<input type="hidden" name="price_key" value="<?php echo esc_attr( $price_key ); ?>" />
											<?php
												$button   = '<a href="%s" data-parent-id="pricing-' . esc_attr( $price_key ) . '" class="btn add-to-cart-btn btn-primary btn-sm btn-inverse">%s</a>';
												$cart_url = add_query_arg( 'trip_id', get_the_ID(), wp_travel_get_cart_url() );
											if ( 'yes' !== $fixed_departure ) :
												$cart_url = add_query_arg( 'trip_duration', $trip_duration, $cart_url );
												?>
													<input type="hidden" name="trip_duration" value="<?php echo esc_attr( $trip_duration ); ?>" />
												<?php
												endif;

												$cart_url = add_query_arg( 'price_key', $price_key, $cart_url );
												printf( $button, esc_url( $cart_url ), esc_html__( 'Book now', 'wp-travel' ) );
											?>
										</div>
									</div>
								</div>
							</li>
													<?php
						}
					endforeach;
					?>
				</ul>
			</div>
		</div>
		<?php

	} elseif ( is_numeric( $trip_data ) ) { // Single Pricing
		$inventory_data = array(
			'status_message' => __( 'N/A', 'wp-travel' ),
			'sold_out'       => false,
			'available_pax'  => 0,
			'booked_pax'     => 0,
			'pax_limit'      => 0,
			'min_pax'        => '',
			'max_pax'        => 0,
		);

		$inventory_data = apply_filters( 'wp_travel_inventory_data', $inventory_data, $trip_id, '' );

		$pricing_status_msg = $inventory_data['status_message'];
		$pricing_sold_out   = $inventory_data['sold_out'];
		$available_pax      = $inventory_data['available_pax'];
		$booked_pax         = $inventory_data['booked_pax'];
		$pax_limit          = $inventory_data['pax_limit'];
		$min_pax            = $inventory_data['min_pax'];
		$max_pax            = $inventory_data['max_pax'];
		?>
		<div id="wp-travel-date-price" class="detail-content">

			<div class="availabily-wrapper">

				<ul class="availabily-list <?php echo 'yes' === $fixed_departure ? 'additional-col' : ''; ?>">

					<li class="availabily-content clearfix" id="trip-duration-content">
						<?php if ( 'yes' == $fixed_departure ) : ?>
							<div class="date-from">
								<span class="availabily-heading-label"><?php echo esc_html__( 'start:', 'wp-travel' ); ?></span>
								<?php echo esc_html( date_i18n( 'l', strtotime( $trip_start_date ) ) ); ?>
								<?php $date_format = get_option( 'date_format' ); ?>
								<?php if ( ! $date_format ) : ?>
									<?php $date_format = 'jS M, Y'; ?>
								<?php endif; ?>
								<span><?php echo esc_html( date_i18n( $date_format, strtotime( $trip_start_date ) ) ); ?></span>
								<input type="hidden" name="trip_date" value="<?php echo esc_attr( $trip_start_date ); ?>">
							</div>
							<?php
							if ( $show_end_date ) :
								?>
								<div class="date-to">
									<?php if ( '' !== $trip_end_date ) : ?>
										<span class="availabily-heading-label"><?php echo esc_html__( 'end:', 'wp-travel' ); ?></span>
										<?php echo esc_html( date_i18n( 'l', strtotime( $trip_end_date ) ) ); ?>
										<span><?php echo esc_html( date_i18n( $date_format, strtotime( $trip_end_date ) ) ); ?></span>
										<input type="hidden" name="trip_departure_date" value="<?php echo esc_attr( $trip_end_date ); ?>">
									<?php else : ?>
										<?php esc_html_e( '-', 'wp-travel' ); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						<?php else : ?>
							<div class="date-from">
								<span class="availabily-heading-label"><?php echo esc_html__( 'start:', 'wp-travel' ); ?></span>
								<?php
								$total_days = 0;
								if ( $trip_duration > 0 || $trip_duration_night > 0 ) {
									$days = $trip_duration > $trip_duration_night ? $trip_duration : $trip_duration_night;
									$days--; // As we need to exclude current selected date.
									$total_days = $days ? $days : $total_days;
								}
								$start_field = array(
									'label'         => esc_html__( 'start', 'wp-travel' ),
									'type'          => 'date',
									'name'          => 'trip_date',
									'placeholder'   => esc_html__( 'Arrival date', 'wp-travel' ),
									'class'         => 'wp-travel-pricing-days-night',
									'validations'   => array(
										'required' => true,
									),
									'attributes'    => array(
										'data-parsley-trigger' => 'change',
										'data-parsley-required-message' => esc_attr__( 'Please Select a Date', 'wp-travel' ),
										'data-totaldays' => $total_days,
									),
									'wrapper_class' => 'date-from',
								);
								$form_field->init()->render_input( $start_field );
								?>
							</div>
							<div class="date-to">
								<?php
								$end_field = array(
									'label'       => esc_html__( 'End', 'wp-travel' ),
									'type'        => 'date',
									'name'        => 'trip_departure_date',
									'placeholder' => esc_html__( 'Departure date', 'wp-travel' ),
								);
								$end_field = wp_parse_args( $end_field, $start_field );
								$form_field->init()->render_input( $end_field );
								?>
							</div>
						<?php endif; ?>
							<div class="number-travellers">
									<div class="inupt-group">
											<label for="number-travellers" class="number-travellers">How many travellers:</label>
											<div class="right-number">
												<span class="plus"></span>
												
												<input type="number" id="number-travellers" name="pax" class="form-control" value="1" minvalue="1">

												<span class="minus"></span>
											</div>
										</div>
								</div>
								<div class="price-regular-wrap">
											

						<?php
						if ( class_exists( 'WP_Travel_Util_Inventory' ) && ! $trip_price ) :
							// display price unavailable text
							$no_price_text = isset( $settings['price_unavailable_text'] ) && '' !== $settings['price_unavailable_text'] ? $settings['price_unavailable_text'] : '';
							echo '<div class="price"><strong>' . esc_html( $no_price_text ) . '</strong></div>';
						else :
							?>

							<div class="price">
								<?php if ( $enable_sale ) : ?>
									<del>
										<span><?php echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ', $currency_symbol, $trip_price ), $currency_symbol, $trip_price ); ?></span>
									</del>
								<?php endif; ?>
								<span class="person-count">
									<ins>
										<span>
											<?php
											if ( $enable_sale ) {
												echo apply_filters( 'wp_travel_itinerary_sale_price', sprintf( ' %s %s', $currency_symbol, $sale_price ), $currency_symbol, $sale_price );
											} else {
												echo apply_filters( 'wp_travel_itinerary_price', sprintf( ' %s %s ', $currency_symbol, $trip_price ), $currency_symbol, $trip_price );
											}
											?>
										</span>
									</ins>
								</span>
							</div>
						</div>
						<?php endif; ?>
						<?php if ( $enable_sale ) : ?>

							<div class="discount-wrap">
									<span class="discount"><?php echo round(100-($sale_price/$trip_price)*100); ?>%</span>
									<span class="discount-exit-date">Discount is availabel untill <?php echo $trip_end_date; ?> </span>
							</div>

						<?php endif; ?>
						<div class="tour-btn">
						
						<div class="action">
							<?php
							// if ( $inventory_enabled && $general_sold_out ) :
							if ( $general_sold_out ) :
								?>

								<p class="wp-travel-sold-out"><?php echo $sold_out_btn_rep_msg; ?></p>

							<?php else : ?>
								<?php ?>

									<input type="hidden" name="trip_id" value="<?php echo esc_attr( $trip_id ); ?>">
									<input type="hidden" name="trip_duration" value="<?php echo esc_attr( $trip_duration ); ?>">
									<input type="hidden" name="trip_duration_night" value="<?php echo esc_attr( $trip_duration_night ); ?>">
									<?php
										$button   = '<a href="%s" class="btn btn-primary add-to-cart-btn btn-sm btn-inverse" data-parent-id="trip-duration-content">%s</a>';
										$cart_url = add_query_arg( 'trip_id', get_the_ID(), wp_travel_get_cart_url() );
									if ( 'yes' !== $fixed_departure ) :
										$cart_url = add_query_arg( 'trip_duration', $trip_duration, $cart_url );
										endif;
										printf( $button, esc_url( $cart_url ), esc_html__( 'Book now', 'wp-travel' ) );
									?>
							<?php endif; ?>
						</div></div>
						<div class="wp-travel-booking-row-single-price">
							<?php do_action( 'wp_travel_trip_extras' ); ?>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<?php
	}

}


add_action( 'wp_travel_booking_princing_options_list', 'wp_travel_booking_tab_pricing_options_list_new' );


function wp_travel_single_location_new( $post_id ) {
	if ( ! $post_id ) {
		return;
	}
	$terms = get_the_terms( $post_id, 'travel_locations' );
	$location = $terms[count( $terms )-1]->slug;
	echo $location;

}

function my_enqueue() {

    wp_enqueue_script( 'ajax-script', get_stylesheet_directory_uri() . '/assets/js/map.js', array('jquery') );

    wp_localize_script( 'ajax-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue' );

add_action('wp_ajax_tour_location_map', 'get_tour_location_map'); //fire get_more_posts on AJAX call for logged-in users;
add_action('wp_ajax_nopriv_tour_location_map', 'get_tour_location_map'); //fire get_more_posts on AJAX call for all other users;

function get_tour_location_map(){
	$action = $_POST['action'] ? $_POST['action'] : '';
	$loc = $_POST['location'] ? $_POST['location'] : '';
	$result = array();
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'itineraries',
		'tax_query' => array(
		    array (
		        'taxonomy' => 'travel_locations',
		        'field' => 'slug',
		        'terms' => $loc,
		    )
		),						
	);

	$location_query = new WP_Query( $args );

	$location = get_term_by('slug', $loc, 'travel_locations');
	$image_id = get_term_meta( $location->term_id, 'wp_travel_trip_type_image_id', true );
	$image_meta            = get_post_meta( $image_id, '_wp_attachment_metadata', true );
	$result['toutImgUrl'] = wp_get_attachment_url( $image_id );
	$result['tourTitle']  = $location->name;
	$result['tourExcept'] = $location->description;
	$result['tourCount']  = $location_query->post_count;
	$result['tourLink']   = get_term_link($location->term_id);
	echo wp_json_encode($result);

	
	wp_die();
   
 


}
