<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Travel_Log
 */

get_header(); ?>
	
	
<section class="page-content blog page-blog">
	<div class="container">
		<div class="row">
			<div class="flex-grid">
				<div class="simple-post-content">
							<?php
							while ( have_posts() ) : the_post(); ?>

								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<header class="entry-header header-simple-post">
										<?php travel_log_entry_header() ?>
									</header>
							

									<div class="post-content">
										<?php
											the_content( sprintf(
												/* translators: %s: Name of current post. */
												wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'travel-log' ), array( 'span' => array( 'class' => array() ) ) ),
												the_title( '<span class="screen-reader-text">"', '"</span>', false )
											) );

											wp_link_pages( array(
												'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'travel-log' ),
												'after'  => '</div>',
											) );
										?>
									</div><!-- .entry-content -->

								<footer class="footer-simple-post">
									<div class="footer-left">
										<div class="btn-back">
											<form>
												<a href="#" class="btn btn-alt" onClick="history.back()">
												<i class="material-icons">arrow_back</i>Back to list</a>
											</form>
										</div>
									</div>
									<div class="footer-right">
										<h4 class="title-tags">Tags:</h4>
										<?php the_tags( '<ul class="tags-list"><li class="tags-item">','</li><li class="tags-item">','</li></ul>'); ?>
										
									</div>
								</footer>
								
								</article><!-- #post-## -->
								<?php
								
							endwhile; // End of the loop.
							?>
						</div>
					<div class="blog-posts top-blog">
					<?php
					/**
					 * Hook - travel_log_sidebar.
					 *
					 * @hooked travel_log_add_sidebar -  10
					 */
					do_action( 'travel_log_sidebar' );
					?>
				</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
