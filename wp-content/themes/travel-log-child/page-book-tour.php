<?php
/**
 * Template Name: Tour-list
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Travel_Log
 */

get_header( 'itinerary' );
		?>
	<section class="page-content page-book-tour best-sellers" >
		<div class="list-tors">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">Best sellers</div>
						</div>
					</div>
					<div class="row">
						<?php 	echo  do_shortcode ('[WP_TRAVEL_ITINERARIES type=featured limit=3]'); ?>
					</div>					
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">Moscow tour</div>
						</div>
					</div>
					<div class="row">
						<?php 	echo  do_shortcode ('[WP_TRAVEL_ITINERARIES type = travel_locations slug = "moscow"]'); ?>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">St.Petersburg tour</div>
						</div>
					</div>
					<div class="row">
						<?php 	echo  do_shortcode ('[WP_TRAVEL_ITINERARIES type = travel_locations slug = "st-petersburg"]');  ?>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="section-title">SIBERIAN TOUR</div>
						</div>
					</div>
					<div class="row">
						<?php 		echo  do_shortcode ('[WP_TRAVEL_ITINERARIES type = travel_locations slug = "siberian"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
			<section class="feedback">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title"><?php echo get_field('feedback_title','14'); ?></div>
					</div>
				</div>
				<div class="row">

					<ul class="slider-feedback"id="slider-feedback">
						<?php 
						$feedback_list = get_field('feedback_list', '14');
							foreach ($feedback_list as $feedback) {
								$img_url = get_the_post_thumbnail_url($feedback->ID,'full');
								$locations =get_field('feedback_list', $feedback->ID);
							?>
							<li class="feedback-item">
							<div class="feedback-item-wrap">
								<div class="feedback-img-wrap">	
									<a href="<?php the_permalink($feedback->ID);?>"  target="_blank">
										<img src="<?php echo $img_url; ?>">
									</a>
								</div>
								<div class="feedback-content-wrap">
									<?php
									$locations = get_field( 'feedback_location', $feedback->ID );
									?>
									<div class="feedback-title"><?php  echo $feedback->post_title; ?></div>
									<div class="feedback-desc"> <?php echo $locations; ?></div>
									<a href="<?php the_permalink($feedback->ID);?>"  target="_blank">
										<div class="feedback-except"><?php echo $feedback->post_content; ?></div>
									</a>
								</div>
							</div>
						</li>
					<?php } ?>
	
					</ul>
				</div>
			</div>
		</section>
	<?php

	get_footer( 'itinerary' ); ?>
