   
(function () {
	jQuery(document).ready(function() {
   
	jQuery(".maping-bg>svg").click(function(e){
   		var topBlock; 
      var tour_data = {};
   		if ((e.offsetY-window.innerHeight/2) > 50) {
   			 topBlock =e.offsetY-window.innerHeight/2;
   		}else{
   			topBlock = 100;
   		}
    	var id_path = document.getElementById(e.target.id);
   		if ( id_path  ) {

   			jQuery(".map-1").css({ opacity:  0 });
   			jQuery(e.target).css({ opacity:  1});
        jQuery(".gallery-tour-wrap").hide();
     
  
         jQuery.ajax(
            {
              type: "POST",
              dataType: "json",
              url: my_ajax_object.ajax_url,
              data: {
                action: 'tour_location_map',
                location: e.target.id
            },
            
            success: function(response){
          //      console.log(response.tourCount);
            	if (parseInt(response.tourCount) > 0 ) { 
	                jQuery("#top-news-img").attr('src', response.toutImgUrl); ;
	                jQuery("#top-news-title").text(response.tourTitle);
	                jQuery("#except-tour").text (response.tourExcept);
	                jQuery("#count-tour").text(response.tourCount);
						                console.log(  response.toutImgUrl );

	                jQuery(".gallery-tour-wrap").show();              
	                jQuery(".gallery-tour-wrap").css({ top:  topBlock, left: e.offsetX+200, });
	            }
           	
            }, 
            error: function(errorThrown){
                console.log(errorThrown);
            } 
         });
       } else {
           jQuery(".gallery-tour-wrap").hide();
        }
    });


});
	})(jQuery);