<?php
/**
 * Itinerary Shortcode Contnet Template
 *
 * This template can be overridden by copying it to yourtheme/wp-travel/shortcode/itinerary-item.php.
 *
 * HOWEVER, on occasion wp-travel will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.wensolutions.com/document/template-structure/
 * @author      WenSolutions
 * @package     wp-travel/Templates
 * @since       1.0.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( post_password_required() ) {
	echo get_the_password_form();
	return;
}
	global $wp_travel_itinerary;

$post_id 		= get_the_ID();
$enable_sale 	= get_post_meta( $post_id, 'wp_travel_enable_sale', true );
$trip_price 	= wp_travel_get_trip_price( $post_id );
$sale_price 	= wp_travel_get_trip_sale_price( $post_id ); 
$start_date 	= get_post_meta( $post_id, 'wp_travel_start_date', true );
$end_date 		= get_post_meta( $post_id, 'wp_travel_end_date', true ); 
$settings      = wp_travel_get_settings();
	$currency_code = ( isset( $settings['currency'] ) ) ? $settings['currency'] : '';

	$currency_symbol = wp_travel_get_currency_symbol( $currency_code );?>
<li>
<div class="tour-wrap">
	<?php if ( $enable_sale ) :
		$star_icon = 'star-icon';
	endif; ?>
								<div class="tour-wrap-img <?php echo $star_icon; ?>">
									<a href="<?php the_permalink() ?>" target="_blank"> <?php echo wp_travel_get_post_thumbnail( $post_id, 'wp_travel_thumbnail' ); ?></a>
								</div>
								<div class="tour-content">
									<a href="<?php the_permalink() ?>" class="tour-name" target="_blank"><?php the_title(); ?></a>
									<div class="tour-city">
										<?php $terms = get_the_terms( get_the_ID(), 'travel_locations' ); ?>
								<?php if ( is_array( $terms ) && count( $terms ) > 0 ) : 
									 for ($i = count( $terms )-1; $i>=0; --$i ) { 
									
													$term_name = $terms[$i]->name;
													$term_link = get_term_link( $terms[$i]->term_id, 'travel_locations' ); 
													?>

												<a href="<?php echo get_term_link( $terms[$i]->term_id, 'travel_locations' ); ?>" target="_blank">
													<?php echo esc_html( $term_name ); if ($i>0) echo ', '; ?>
												</a>
											<?php 	
											 }  
										endif; ?>

									</div>
									<div class="tour-desc"><?php the_excerpt(); ?></div>
									<div class="tour-meta-info">
										<div class="price-regular-wrap">
											<span class="price-icon"></span>
											<div class="price">
												<?php if ( $enable_sale ) { ?><span class="price-sale"><?php echo $sale_price.$currency_symbol; ?></span> <?php } ?>
												<span class="price-regular"><?php echo $trip_price.$currency_symbol; ?></span>
											</div>
											<?php if ( $enable_sale ) { ?>
											<div class="discount-wrap"><span class="discount">30%</span></div>
											<span class="discount-exit-date">Discount is availabel untill <?php echo $end_date; ?> </span>
										<?php } ?>
										</div>
										<div class="end-day">
											<span class="calendar-icon"></span>
											<span class="countend-day"><?php wp_travel_get_trip_duration( $post_id ); ?></span>
										</div>
									</div>
									<div class="tour-btn">
										<a href="<?php the_permalink() ?>" class="btn btn-primary" target="_blank">Book a tour</a>
									</div>
								</div>
							</div>
						</li>